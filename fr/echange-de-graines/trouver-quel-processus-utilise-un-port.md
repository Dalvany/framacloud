# Trouver quel processus utilise un port

J'ai longtemps utilisé `ss -taupen | grep <port>` pour trouver quel processus utilisait un port, mais cela a l'inconvénient de retourner aussi les lignes dont le PID (ou n'importe quoi d'autre) matche le numéro de port.

Alors bien sûr, on peut adapter la regex passée à `grep`, mais bon, pourquoi bidouiller quand on a un outil parfait pour ce job ? `lsof -i :<port>` fournira la réponse bien mieux présentée :-)

Bonus : `lsof -i TCP` et `lsof -i UDP` fourniront l'ensemble des processus utilisant respectivement TCP et UDP